'use strict';
/*
    В этом задании нельзя менять разметку, только писать код в этом файле.
 */

/**
*   1. Удали со страницы элемент с id "deleteMe"
**/

function removeBlock() {
    let el = document.getElementById('deleteMe')
    el.parentNode.removeChild(el)
}

/**
 *  2. Сделай так, чтобы во всех элементах с классом wrapper остался только один параграф,
 *  в котором будет сумма чисел из всех параграфов.
 *
 *  Например, такой элемент:
 *
 *  <div class="wrapper"><p>5</p><p>15</p><p>25</p><p>35</p></div>
 *
 *  должен стать таким
 *
 *  <div class="wrapper"><p>80</p></div>
 */

function calcParagraphs() {
    let sumArray = []
    document.querySelectorAll('.wrapper').forEach(wrapper => {
        let sum = 0
        wrapper.querySelectorAll('p').forEach(p => sum += +p.textContent)
        sumArray.push(sum)
    })
    document.querySelectorAll('.wrapper').forEach(wrapper => wrapper.innerHTML = `<p>${sumArray.shift()}</p>`)
}

/**
 *  3. Измени value и type у input c id "changeMe"
 *
 */

function changeInput() {
    let el = document.querySelector('input#changeMe')
    el.setAttribute('value', '1234')
    el.type = 'number'  
}

/**
 *  4. Используя функции appendChild и insertBefore дополни список с id "changeChild"
 *  чтобы получилась последовательность <li>0</li><li>1</li><li>2</li><li>3</li>
 *
 */

function appendList() {
    let newEl1 = document.createElement('li')
    let newEl2 = document.createElement('li')
    newEl1.textContent = '1'
    newEl2.textContent = '3'
    
    let el = document.querySelector('#changeChild')
    el.insertBefore(newEl1, el.lastElementChild)
    el.appendChild(newEl2)
}

/**
 *  5. Поменяй цвет всем div с class "item".
 *  Если у блока class "red", то замени его на "blue" и наоборот для блоков с class "blue"
 */

function changeColors() {
    document.querySelectorAll('.item.red, .item.blue').forEach(item => {
        item.classList.toggle('red')
        item.classList.toggle('blue')
    })
}

removeBlock();
calcParagraphs();
changeInput();
appendList();
changeColors();